# Class for controller
class NumberController < ApplicationController
  def input; end

  def get_binary(num)
    count = 1
    res = 0
    while num.positive? do
      res += count * (num % 2)
      count *= 10
      num = num.div(2)
    end
    res
  end

  def get_length(num)
    result = []
    while num .positive?
      result.push(num % 10)
      num = num.div(10)
    end
    result
  end

  def pal?(num)
    data = get_length(num)
    0.upto(data.length - 1) do |i|
      return false if data[i] != data[data.length - 1 - i]
    end
    true
  end

  def ajax
    @result = []
    begin
      number = Integer(params[:value])
      1.upto(number) do |num|
        pair = []
       bin_value = get_binary(num)
        pair.push(num).push(bin_value) if pal?(bin_value) && pal?(num)
       @result.push(pair) if pair.length == 2
      end
    rescue StandardError
    end
    @result.push(%w[- -]) if @result.length.zero?
    respond_to do |format|
      format.json { render json: { result: @result } }
    end
  end
end
